﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using WSE.Services;

namespace WSE
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {
            MainQueue mainQueue = new MainQueue("https://wp.pl");

            UrlFilterService fs = new UrlFilterService(mainQueue.Dequeue());

            for (int i = 0; i < 1; i++)
            {
                bool allowed;
                try
                {
                    allowed = fs.IsAllowed(await GetUrlResponseAsString(new Uri(fs.robotsUrl)));
                }
                catch
                {
                    allowed = false;
                }

                if (allowed)
                {
                    string currHtml = await GetUrlResponseAsString(new Uri(fs.startingUrl));
                    WebscrapperService ws = new WebscrapperService(fs.startingUrl, fs.domain, currHtml);
                    DbAccessService db = new DbAccessService();
                    db.UploadSiteRecordToDb(ws.sr, db);
                    foreach (string link in ws.sr.LinksOut)
                    {
                        mainQueue.Enqueue(link);
                    }

                    fs.Next(mainQueue.Dequeue());
                    continue;
                }
                else
                {
                    if (mainQueue.Count() > 0)
                    {
                        fs.Next(mainQueue.Dequeue());
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("Jobs completed. Queue empty.");
                        return;
                    }
                };
            }

            // Part 2 - PageRank - When loop ends: 
            PageRankService.GetPageRankService();
        }

        static async Task<string> GetUrlResponseAsString(Uri url)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseBody);
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(" Exception Message :{0} ", e.Message);
                return "";
            }
        }
    }
}
