﻿using System;
using System.Collections.Generic;
using System.Text;
using HtmlAgilityPack;
using WSE.DataStructures;

namespace WSE.Services
{
    class WebscrapperService
    {
        private HtmlDocument _html = new HtmlDocument();
        public SiteRecord sr;

        public WebscrapperService(string url, string domain, string htmlStringified)
        {
            _html.LoadHtml(htmlStringified);
            sr = WebscrapFromHtml(_html, url, domain);
        }

        private static SiteRecord WebscrapFromHtml(HtmlDocument html, string startingUrl, string domain)
        {
            var metaTags = html.DocumentNode.SelectNodes("//meta");
            SiteRecord siteData = new SiteRecord(startingUrl);
            siteData.Domain = domain;

            if (metaTags != null)
            {
                int matchCount = 0;
                foreach (var tag in metaTags)
                {
                    var tagName = tag.Attributes["name"];
                    var tagContent = tag.Attributes["content"];
                    var tagProperty = tag.Attributes["property"];

                    if (tagName != null && tagContent != null)
                    {

                        switch (tagName.Value.ToLower())
                        {
                            case "title":
                                siteData.Title = tagContent.Value;
                                matchCount++;
                                break;
                            case "description":
                                siteData.Description = tagContent.Value;
                                matchCount++;
                                break;
                            case "keywords":
                                string[] words = tagContent.Value.Split();
                                foreach (string word in words)
                                {
                                    if (word.Length > 20)
                                    {
                                        string[] tokens = word.Split(',');
                                        words = tokens;
                                        continue;
                                    }

                                    var reworkedWord = word.ToLower().Trim();
                                    if (reworkedWord.EndsWith(',') || reworkedWord.EndsWith('.'))
                                    {
                                        siteData.Keywords.Add(reworkedWord.Substring(0, reworkedWord.Length - 1));
                                    }
                                    else
                                    {
                                        siteData.Keywords.Add(reworkedWord);
                                    }
                                }
                                matchCount++;
                                break;
                        }
                    }
                    else if (tagProperty != null && tagContent != null)
                    {
                        switch (tagProperty.Value.ToLower())
                        {
                            case "og:title":
                                siteData.Title = string.IsNullOrEmpty(siteData.Title) ? tagContent.Value : siteData.Title;
                                matchCount++;
                                break;
                            case "og:description":
                                siteData.Description = string.IsNullOrEmpty(siteData.Description) ? tagContent.Value : siteData.Description;
                                matchCount++;
                                break;
                        }
                    }
                    siteData.HasData = matchCount > 0;
                }

                if (siteData.HasData && siteData.Domain != null)
                {
                    var AllSiteHrefs = ExtractHrefTags(html);
                    foreach (string link in AllSiteHrefs)
                    {
                        siteData.LinksOut.Add(link);
                    }
                    siteData.DomainsOut = ExtractUniqueDomains(AllSiteHrefs, siteData.Domain);
                }
            }
            return siteData;
        }

        public static List<string> ExtractHrefTags(HtmlDocument htmlSnippet)
        {
            var hrefTags = new List<string>();
            var nodeList = htmlSnippet.DocumentNode.SelectNodes("//a[@href]");

            if (nodeList != null)
            {
                foreach (HtmlNode node in nodeList)
                {
                    HtmlAttribute att = node.Attributes["href"];
                    hrefTags.Add(att.Value);
                }
            }

            return hrefTags;
        }

        public static List<string> ExtractUniqueDomains(List<string> hrefs, string currentDomain)
        {
            var uniqDomains = new List<string>();
            uniqDomains.Add(currentDomain);

            foreach (string link in hrefs)
            {
                string currDomain;
                try
                {
                    currDomain = ExtractDomain(link);
                }
                catch
                {
                    currDomain = "";
                }
                
                if (currDomain.StartsWith("www.") && currDomain.Length > 0)
                {
                    currDomain = currDomain.Remove(0, 4);
                }
                if (!uniqDomains.Contains(currDomain) && currDomain.Length > 0)
                {
                    uniqDomains.Add(currDomain);
                }
            }
            uniqDomains.Remove(currentDomain);
            return uniqDomains;
        }

        public static string ExtractDomain(string url)
        {
            Uri uriAddress = new Uri(url);
            return uriAddress.Host;
        }

    }
}
