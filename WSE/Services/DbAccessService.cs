﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using WSE.DataStructures;

namespace WSE.Services
{
    class DbAccessService
    {
        string connectionString = "Server=(LocalDb)\\localdb;Database=sitesDb;Trusted_Connection=True;";
        public List<AddressData> GetAddresses()
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<AddressData>("select * from dbo.Addresses").AsList();
            }
        }

        public void InsertAddress(AddressData address, string addressDomain)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select Addresses.Link from dbo.Addresses where Link = '{address.Link}'";
                var linkList = connection.Query<DomainData>(sql).AsList();
                if (linkList.Count < 1 && address.Link.Length > 1)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        var checkedDomainIDList = connection.Query<DomainData>($"select Domains.DomainID from dbo.Domains where Domain = '{addressDomain}'").AsList();
                        if (checkedDomainIDList.Count > 0)
                        {
                            sql = $"INSERT INTO [sitesDb].[dbo].[Addresses] ([Link],[Title],[PageRank],[DomainID]) VALUES('{address.Link}','{address.Title}',{address.PageRank},{checkedDomainIDList[0].DomainID})";
                            connection.Execute(sql);
                            i++;
                        }
                        else
                        {
                            string sqlInsert = $"INSERT INTO [sitesDb].[dbo].[Domains] ([Domain]) VALUES('{addressDomain}')";
                            connection.Execute(sqlInsert);
                        }
                    }
                }
            }
        }

        public int GetAddressId(string address)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select top 1 AddressID from dbo.Addresses where Link = '{address}'";
                var addressesList = connection.Query<AddressData>(sql).AsList();
                if (addressesList.Count > 0)
                {
                    return addressesList[0].AddressID;
                }
                return -1;
            }
        }

        public List<DomainData> GetDomains()
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<DomainData>("select * from dbo.Domains").AsList();
            }
        }

        public int[] GetAllDomainIDs()
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var domainDataList = connection.Query<DomainData>("select DomainID from dbo.Domains").AsList();
                var domainIDList = new List<int>();
                foreach (var domainID in domainDataList)
                {
                    domainIDList.Add(domainID.DomainID);
                }

                return domainIDList.ToArray();
            }
        }

        public void InsertDomain(DomainData domain)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select Domains.Domain from dbo.Domains where Domain = '{domain.Domain}'";
                var domainsList = connection.Query<DomainData>(sql).AsList();
                if (domainsList.Count < 1 && domain.Domain.Length > 1)
                {
                    string sqlInsert = $"INSERT INTO [sitesDb].[dbo].[Domains] ([Domain]) VALUES('{domain.Domain}')";
                    connection.Execute(sqlInsert);
                }
            }
        }

        public int GetDomainId(string domain)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select top 1 DomainID from dbo.Domains where Domain = '{domain}'";
                var domainsList = connection.Query<DomainData>(sql).AsList();
                if (domainsList.Count > 0)
                {
                    return domainsList[0].DomainID;
                }
                return -1;
            }
        }

        public List<KeywordData> GetKeywords()
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<KeywordData>("select * from dbo.Keywords").AsList();
            }
        }

        public void InsertKeyword(KeywordData keyword)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select Keywords.Keyword from dbo.Keywords where Keyword = '{keyword.Keyword}'";
                var keywordsList = connection.Query<DomainData>(sql).AsList();
                if (keywordsList.Count < 1 && keyword.Keyword.Length > 1)
                {
                    sql = $"INSERT INTO [sitesDb].[dbo].[Keywords] ([Keyword]) VALUES('{keyword.Keyword}')";
                    connection.Execute(sql);
                }
            }
        }

        public int GetKeywordId(string keyword)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select top 1 KeywordID from dbo.Keywords where Keyword = '{keyword}'";
                var keywordList = connection.Query<KeywordData>(sql).AsList();
                if (keywordList.Count > 0)
                {
                    return keywordList[0].KeywordID;
                }
                return -1;
            }
        }

        public List<AddressDomainsOutData> GetAddressDomainsOut(int addressId)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<AddressDomainsOutData>($"select * from dbo.AddressDomainsOut where AddressID = '{addressId}'").AsList();
            }
        }

        public void InsertAddressDomainReference(AddressDomainsOutData data)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select * from dbo.AddressDomainsOut where AddressID = {data.AddressID} and DomainID = {data.DomainID}";
                var addressDomainOutRelationList = connection.Query<AddressDomainsOutData>(sql).AsList();
                if (addressDomainOutRelationList.Count > 0) return;
                sql = $"INSERT INTO [sitesDb].[dbo].[AddressDomainsOut] VALUES ({data.AddressID},{data.DomainID})";
                connection.Execute(sql);
            }

        }

        public List<AddressKeywordsData> GetAddressKeywords(int addressId)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                return connection.Query<AddressKeywordsData>($"select * from dbo.AddressDomainsOut where AddressID = '{addressId}'").AsList();
            }
        }

        public void InserdAddressKeyword(AddressKeywordsData data)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select * from dbo.AddressKeywords where AddressID = {data.AddressID} and KeywordID = {data.KeywordID}";
                var addressKeywordRelationList = connection.Query<AddressKeywordsData>(sql).AsList();
                if (addressKeywordRelationList.Count > 0) return;
                sql = $"INSERT INTO [sitesDb].[dbo].[AddressKeywords] VALUES ({data.AddressID},{data.KeywordID})";
                connection.Execute(sql);
            }
        }

        public List<DomainDomainsRefsData> GetDomainIDReferedDomainIDs(int domainID)
        {
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var sql = $"select Addresses.DomainID, AddressDomainsOut.DomainID from AddressDomainsOut INNER JOIN Addresses ON AddressDomainsOut.AddressID = Addresses.AddressID WHERE Addresses.DomainID = {domainID}";
                var domainDomainsRefsList = connection.Query<DomainDomainsRefsData>(sql).AsList();
                return domainDomainsRefsList;
            }
        }

        public void UpdatePageRank(List<DomainPageRankDataNormalized> rankDatas)
        {
            foreach (var data in rankDatas)
            {
                using (IDbConnection connection = new SqlConnection(connectionString))
                {
                    string rank = data.PageRank.ToString("0.000");
                    var sql = $"UPDATE Addresses SET PageRank = CAST('{rank}' AS decimal(4,4)) WHERE Addresses.DomainID = {data.DomainID}";
                    connection.Execute(sql);
                }
            }
        }

        public void UploadSiteRecordToDb(SiteRecord data, DbAccessService db)
        {
            // Send AddressDomain
            db.InsertDomain(new DomainData
            {
                Domain = data.Domain
            });

            // Send Domains found to dbo.Domain
            foreach (string domain in data.DomainsOut)
            {
                db.InsertDomain(new DomainData
                {
                    Domain = domain
                });
            }

            // Send Keywords
            foreach (string keyword in data.Keywords)
            {
                db.InsertKeyword(new KeywordData
                {
                    Keyword = keyword
                });
            }

            // Send Address
            db.InsertAddress(new AddressData
            {
                Link = data.LinkIn,
                Title = data.Title,
                PageRank = data.PageRank
            }, data.Domain);

            // Send Relation AddressDomains
            foreach (string domain in data.DomainsOut)
            {
                var linkId = db.GetAddressId(data.LinkIn);
                var domainId = db.GetDomainId(domain);

                if (linkId > 0 && domainId > 0)
                {
                    db.InsertAddressDomainReference(new AddressDomainsOutData
                    {
                        AddressID = linkId,
                        DomainID = domainId
                    });
                }
            }

            // Send Relation AddressKeywords
            foreach (string keyword in data.Keywords)
            {
                var linkId = db.GetAddressId(data.LinkIn);
                var keywordId = db.GetKeywordId(keyword);

                if (linkId > 0 && keywordId > 0)
                {
                    db.InserdAddressKeyword(new AddressKeywordsData
                    {
                        AddressID = linkId,
                        KeywordID = keywordId
                    });
                }
            }
        }
    }
}
