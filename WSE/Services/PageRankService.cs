﻿using System;
using System.Collections.Generic;
using System.Text;
using WSE.DataStructures;

namespace WSE.Services
{
    class PageRankService
    {
        protected DbAccessService db = new DbAccessService();
        private List<DomainPageRankDataNormalized> _rankToDb;
        private List<List<decimal>> _domainMatrix;
        private List<decimal> _vector, _PageRank;
        private int[] _domainIDList;

        private static PageRankService instance {get; set;}

        private PageRankService()
        {
            _domainIDList = db.GetAllDomainIDs();
            _domainMatrix = GetDomainMatrix(_domainIDList);
            _vector = CreateVector(_domainMatrix);
            _PageRank = CalculateMatrices(_domainMatrix, _vector, 4);
            _rankToDb = NormalizeDomainPageRank(MergePageRankDomainID(_PageRank, _domainIDList));
            //db.UpdatePageRank(rankToDb);
        }

        public static PageRankService GetPageRankService()
        {
            if (instance == null)
            {
                instance = new PageRankService();
            }
            return instance;
        }

        private List<List<decimal>> GetDomainMatrix(int[] domainIDList)
        {
            List<List<decimal>> domainMatrix = new List<List<decimal>>();

            foreach (int domainID in domainIDList)
            {
                List<decimal> currMatrixCol = new List<decimal>();

                var addressDomainIDVotesList = db.GetDomainIDReferedDomainIDs(domainID);
                var allVotes = addressDomainIDVotesList.Count;

                foreach (int colDomainID in domainIDList)
                {
                    var count = CountDomainIDRefs(addressDomainIDVotesList, colDomainID);

                    if (count > 0)
                    {
                        decimal calc = (decimal)count/(decimal)allVotes;
                        currMatrixCol.Add(calc);
                    }
                    else
                    {
                        currMatrixCol.Add(0m);
                    }
                }

                domainMatrix.Add(currMatrixCol);
            }

            return domainMatrix;
        }

        private List<decimal> CreateVector(List<List<decimal>> domainMatrix)
        {
            List<decimal> vector = new List<decimal>();
            var matrixSize = domainMatrix.Count;
            var calculatedStartValue = (decimal)1 / (decimal)matrixSize;

            for (int i = 0; i < matrixSize; i++)
            {
                vector.Add(calculatedStartValue);
            }
            Console.WriteLine(vector.Count);
            return vector;
        }

        private int CountDomainIDRefs(List<DomainDomainsRefsData> refs, int domainID)
        {
            var count = 0;
            foreach(var refer in refs)
            {
                if(refer.DomainID == domainID)
                {
                    count++;
                }
            }
            return count;
        }

        private List<decimal> CalculateMatrices(List<List<decimal>> startingMatrix, List<decimal> vectors, int iterations)
        {
            if (iterations < 1) { iterations = 1; }
            List<List<decimal>> matrixPower = CalculateMatrixNPower(startingMatrix, iterations - 1);
            var vectorVar = vectors.ToArray();
            return MultiplyMatricesByVector(matrixPower, vectorVar[0]);
        }

        private List<List<decimal>> CalculateMatrixNPower (List<List<decimal>> matrix, int n)
        {
            int i = n;


            //TBC





















            return matrix;
        }
        private List<decimal> MultiplyMatricesByVector(List<List<decimal>> matrix, decimal vector)
        {
            var matrixArr = new List<decimal[]>();
            //decimal[][] matrixConverted = new decimal[matrixArr.Count][]; 
            var result = new List<decimal>();

            foreach (var column in matrix)
            {
                var newCol = column.ToArray();
                matrixArr.Add(newCol);
            }

            decimal[][] matrixConverted = matrixArr.ToArray();
            int len = matrixConverted.Length;

            for (int i = 0; i < len; i++)
            {
                var addition = 0.0m;
                for (int j = 0; j < len; j++)
                {
                    addition += matrixConverted[i][j] * vector;
                }
                Console.WriteLine(addition);
                result.Add(addition);
            }
            
            return result;
        }

        private decimal SumMultiplication(decimal[] arr, decimal val)
        {
            var result = 0.0m;
            foreach (decimal item in arr)
            {
                result += (item * val);
            }
            return result;
        }

        private List<DomainPageRankData> MergePageRankDomainID(List<decimal> PageRank, int[] domainIDList)
        {
            List<DomainPageRankData> domainIDPageRankList = new List<DomainPageRankData>();
            var pageRankArray = PageRank.ToArray();
            for (int i = 0; i < domainIDList.Length; i++)
            {
                DomainPageRankData entity = new DomainPageRankData
                {
                    DomainID = domainIDList[i],
                    PageRank = pageRankArray[i]
                };
                domainIDPageRankList.Add(entity);
            }
            return domainIDPageRankList;
        }

        private List<DomainPageRankDataNormalized> NormalizeDomainPageRank(List<DomainPageRankData> list)
        {
            List<DomainPageRankDataNormalized> domainIDPageRankListNormalized = new List<DomainPageRankDataNormalized>();

            list.Sort(delegate (DomainPageRankData x, DomainPageRankData y)
            {
                return y.PageRank.CompareTo(x.PageRank);
            });
            var rankingPos = 0.0m;
            foreach (var item in list)
            {
                rankingPos += 0.001m;
                var itemNormalized = new DomainPageRankDataNormalized()
                {
                    DomainID = item.DomainID,
                    PageRank = (double)rankingPos
                };
                domainIDPageRankListNormalized.Add(itemNormalized);
            }

            return domainIDPageRankListNormalized;
        }
    }
}
