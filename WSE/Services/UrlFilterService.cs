﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using WSE.DataStructures;

namespace WSE.Services
{
    class UrlFilterService
    {
        public string domain;
        public string robotsUrl;
        public string startingUrl;
        public UrlFilterService(string url)
        {
            Next(url);
        }

        public string ExtractDomain(string url)
        {
            Uri uriAddress = new Uri(url);
            return uriAddress.Host;
        }

        public string ExtractRobots(string url)
        {
            Uri uriAddress = new Uri(url);
            return $"{uriAddress.Scheme}://{uriAddress.Host}/robots.txt";
        }

        public bool IsAllowed(string response)
        {
            try
            {
                Uri startingUri = new Uri(startingUrl);

                if (response.Length > 0)
                {
                    if (response.Contains("User-agent: *") & response.Contains("Disallow: /\n"))
                    {
                        Console.WriteLine("Filter: Site disallows every robot.");
                        return false;
                    }
                    else
                    {
                        foreach (string segment in startingUri.Segments)
                        {
                            if (segment.Length > 1)
                            {
                                if (response.Contains("User-agent: *") & response.Contains($"{segment}"))
                                {
                                    Console.WriteLine($"Filter: Segment not allowed: {segment}");
                                    return false;
                                }
                            }
                        }
                    }
                }

                Console.WriteLine("Filter: Site allowed.");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Next(string url)
        {
            try
            {
                startingUrl = url;
                domain = ExtractDomain(url);
                robotsUrl = ExtractRobots(url);
            }
            catch
            {
                startingUrl = url;
                domain = "";
                robotsUrl = "";
            }
        }
    }
}
