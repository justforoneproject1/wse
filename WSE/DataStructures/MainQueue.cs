﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace WSE.Services
{
    class MainQueue
    {
        public MainQueue(string url) {
            Enqueue(url);
        }

        private Queue<string> _urlQueue = new Queue<string>();

        public void Enqueue(string initUrl)
        {
            if (IsUrl(initUrl)) {
                _urlQueue.Enqueue(initUrl);
                Console.WriteLine($"Queue: {initUrl}|+");
            } else {
                Console.WriteLine($"Queue: {initUrl}|-");
            }
        }

        public string Dequeue()
        {
            return _urlQueue.Dequeue();
        }

        public int Count()
        {
            return _urlQueue.Count;
        }

        protected bool IsUrl(string url)
        {
            var urlRegExp = new Regex(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$");
            return urlRegExp.IsMatch(url);
        }
    }
}
