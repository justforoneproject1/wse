﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class AddressKeywordsData
    {
        public int AddressID { get; set; }
        public int KeywordID { get; set; }
    }
}
