﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class DomainDomainsRefsData
    {
        public int AddressDomainID { get; set; }
        public int DomainID { get; set; }
    }
}
