﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class SiteRecord
    {
        public bool HasData { get; set; }
        public string LinkIn { get; set; }
        public string Domain { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Keywords { get; set; } = new List<string>();
        public List<string> DomainsOut { get; set; } = new List<string>();

        public List<string> LinksOut { get; set; } = new List<string>();
        public float PageRank{ get; set; }
        public SiteRecord(string url)
        {
            LinkIn = url;
            HasData = false;
            PageRank = 0.0f;
        }

    }
}
