﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class KeywordData
    {
        public int KeywordID { get; set; }
        public string Keyword { get; set; }
    }
}
