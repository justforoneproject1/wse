﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class AddressData
    {
        public int AddressID { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public float PageRank { get; set; }
        public int DomainID { get; set; }
    }
}
