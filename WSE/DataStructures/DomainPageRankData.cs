﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.DataStructures
{
    class DomainPageRankData
    {
        public int DomainID { get; set; }
        public decimal PageRank { get; set; }
    }

    class DomainPageRankDataNormalized : DomainPageRankData
    {
        public new int DomainID { get; set; }
        public new double PageRank { get; set; }
    }
}
